window.__imported__ = window.__imported__ || {};
window.__imported__["prototype_navigation@1x/layers.json.js"] = [
	{
		"objectId": "0D8CB85C-15FE-4E05-9B2B-72FD5704B835",
		"kind": "artboard",
		"name": "Screen_1",
		"originalName": "Screen 1",
		"maskFrame": null,
		"layerFrame": {
			"x": -523,
			"y": 150,
			"width": 1060,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "9E4DE6E4-8ABE-49FA-94BF-BADA92979ABC",
				"kind": "group",
				"name": "Content",
				"originalName": "Content",
				"maskFrame": null,
				"layerFrame": {
					"x": 7,
					"y": 0,
					"width": 1051,
					"height": 856
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Content-ouu0reu2.png",
					"frame": {
						"x": 7,
						"y": 0,
						"width": 1051,
						"height": 856
					}
				},
				"children": [
					{
						"objectId": "C04100F9-763E-4C9D-9CB1-9F1C52AA9685",
						"kind": "group",
						"name": "Group_2_Copy",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 10,
							"width": 665,
							"height": 39
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-qza0mtaw.png",
							"frame": {
								"x": 18,
								"y": 10,
								"width": 665,
								"height": 39
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "D9307C6B-4249-40B2-B695-419EC62277AF",
		"kind": "artboard",
		"name": "Screen_2",
		"originalName": "Screen 2",
		"maskFrame": null,
		"layerFrame": {
			"x": 637,
			"y": 150,
			"width": 1060,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "0B9CAC3F-9550-40C5-B72E-08A40C3F6BA8",
				"kind": "group",
				"name": "Content1",
				"originalName": "Content",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": -2,
					"width": 1237,
					"height": 858
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-Content-mei5q0fd.png",
					"frame": {
						"x": 0,
						"y": -2,
						"width": 1237,
						"height": 858
					}
				},
				"children": [
					{
						"objectId": "8599BAD8-6BD1-424F-94CB-E7EF0426E3A9",
						"kind": "group",
						"name": "add_list_button",
						"originalName": "add-list-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 881,
							"y": 10,
							"width": 67,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-add_list_button-odu5oujb.png",
							"frame": {
								"x": 881,
								"y": 10,
								"width": 67,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "40B250C6-F079-42C1-B3F8-5F94BBDEEFD3",
						"kind": "group",
						"name": "success_secondary_button_resting",
						"originalName": "success-secondary-button--resting",
						"maskFrame": null,
						"layerFrame": {
							"x": 958,
							"y": 10,
							"width": 86,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-success_secondary_button_resting-ndbcmjuw.png",
							"frame": {
								"x": 958,
								"y": 10,
								"width": 86,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "C4005641-1E19-4E32-9195-BF7AE0026938",
						"kind": "group",
						"name": "board_dropdown",
						"originalName": "board-dropdown",
						"maskFrame": null,
						"layerFrame": {
							"x": 180,
							"y": 10,
							"width": 130,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-board_dropdown-qzqwmdu2.png",
							"frame": {
								"x": 180,
								"y": 10,
								"width": 130,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "906CE9A6-1511-4B7E-82D8-7B081EFEC9D1",
						"kind": "group",
						"name": "search_field",
						"originalName": "search-field",
						"maskFrame": null,
						"layerFrame": {
							"x": 320,
							"y": 10,
							"width": 551,
							"height": 33
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-search_field-ota2q0u5.png",
							"frame": {
								"x": 320,
								"y": 10,
								"width": 551,
								"height": 33
							}
						},
						"children": []
					},
					{
						"objectId": "69F002DC-FB1A-4053-81E4-3D797464050A",
						"kind": "group",
						"name": "Group_2_Copy1",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 17,
							"y": 19,
							"width": 152,
							"height": 17
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-njlgmday.png",
							"frame": {
								"x": 17,
								"y": 19,
								"width": 152,
								"height": 17
							}
						},
						"children": []
					}
				]
			}
		]
	},
	{
		"objectId": "53F03699-942F-44AE-8A44-A27CBD9C55D8",
		"kind": "artboard",
		"name": "sidebar",
		"originalName": "sidebar",
		"maskFrame": null,
		"layerFrame": {
			"x": -2193,
			"y": 150,
			"width": 220,
			"height": 750
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "69E30B54-96E6-4ADA-B168-3E4AD60A46CF",
				"kind": "group",
				"name": "bg",
				"originalName": "bg",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 220,
					"height": 750
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-bg-njlfmzbc.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 220,
						"height": 750
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "17591D0B-FBF5-4A30-B101-6A56F7FFB5C6",
		"kind": "artboard",
		"name": "topbar",
		"originalName": "topbar",
		"maskFrame": null,
		"layerFrame": {
			"x": -3610,
			"y": 100,
			"width": 1280,
			"height": 50
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "1E222E4F-C1D1-43F0-A95F-F4E694FC7410",
				"kind": "group",
				"name": "sidebar_button",
				"originalName": "sidebar_button",
				"maskFrame": null,
				"layerFrame": {
					"x": 5,
					"y": 2,
					"width": 40,
					"height": 44
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-sidebar_button-muuymjjf.jpg",
					"frame": {
						"x": 5,
						"y": 2,
						"width": 40,
						"height": 44
					}
				},
				"children": []
			},
			{
				"objectId": "E7496CF0-7FB2-45FE-87AF-3597F775AD8E",
				"kind": "group",
				"name": "gitlab_logo",
				"originalName": "gitlab_logo",
				"maskFrame": null,
				"layerFrame": {
					"x": 49,
					"y": 12,
					"width": 29,
					"height": 26
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-gitlab_logo-rtc0otzd.png",
					"frame": {
						"x": 49,
						"y": 12,
						"width": 29,
						"height": 26
					}
				},
				"children": []
			},
			{
				"objectId": "FA149357-B179-43B6-A952-B05B9C975653",
				"kind": "group",
				"name": "global_breadcrumbs",
				"originalName": "global_breadcrumbs",
				"maskFrame": null,
				"layerFrame": {
					"x": 92,
					"y": 17,
					"width": 322,
					"height": 19
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-global_breadcrumbs-rkexndkz.png",
					"frame": {
						"x": 92,
						"y": 17,
						"width": 322,
						"height": 19
					}
				},
				"children": []
			},
			{
				"objectId": "509127C3-9DDB-4817-AEB1-192A8EE346F0",
				"kind": "group",
				"name": "search_bar",
				"originalName": "search_bar",
				"maskFrame": null,
				"layerFrame": {
					"x": 431,
					"y": 7,
					"width": 244,
					"height": 35
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-search_bar-nta5mti3.png",
					"frame": {
						"x": 431,
						"y": 7,
						"width": 244,
						"height": 35
					}
				},
				"children": []
			},
			{
				"objectId": "73959616-61B1-433D-B6E7-7C7BA20FA0BB",
				"kind": "group",
				"name": "plus_button",
				"originalName": "plus_button",
				"maskFrame": null,
				"layerFrame": {
					"x": 690,
					"y": 17,
					"width": 29,
					"height": 14
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-plus_button-nzm5ntk2.png",
					"frame": {
						"x": 690,
						"y": 17,
						"width": 29,
						"height": 14
					}
				},
				"children": []
			},
			{
				"objectId": "C6344E91-B5A8-49B6-981A-C4F694850857",
				"kind": "group",
				"name": "exploration_buttons",
				"originalName": "exploration_buttons",
				"maskFrame": null,
				"layerFrame": {
					"x": 735,
					"y": 18,
					"width": 289,
					"height": 17
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-exploration_buttons-qzyzndrf.png",
					"frame": {
						"x": 735,
						"y": 18,
						"width": 289,
						"height": 17
					}
				},
				"children": []
			},
			{
				"objectId": "5A27D35B-4C17-4330-A67D-02060B747C93",
				"kind": "group",
				"name": "counters",
				"originalName": "counters",
				"maskFrame": null,
				"layerFrame": {
					"x": 1075,
					"y": -2,
					"width": 145,
					"height": 53
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-counters-nueyn0qz.jpg",
					"frame": {
						"x": 1075,
						"y": -2,
						"width": 145,
						"height": 53
					}
				},
				"children": []
			},
			{
				"objectId": "31A9D7B1-0F3A-4B07-98B6-B1388EFE88E6",
				"kind": "group",
				"name": "profile",
				"originalName": "profile",
				"maskFrame": null,
				"layerFrame": {
					"x": 1227,
					"y": 11,
					"width": 40,
					"height": 26
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-profile-mzfbouq3.png",
					"frame": {
						"x": 1227,
						"y": 11,
						"width": 40,
						"height": 26
					}
				},
				"children": [
					{
						"objectId": "62099829-EB2E-4F77-A64B-459DA7A3B905",
						"kind": "group",
						"name": "user_picture",
						"originalName": "user_picture",
						"maskFrame": {
							"x": 0,
							"y": 0,
							"width": 26,
							"height": 26
						},
						"layerFrame": {
							"x": 1227,
							"y": 11,
							"width": 26,
							"height": 26
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-user_picture-njiwotk4.png",
							"frame": {
								"x": 1227,
								"y": 11,
								"width": 26,
								"height": 26
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "47C87EB3-112E-4D1D-AD81-F29E6CDF2BB1",
				"kind": "group",
				"name": "bg1",
				"originalName": "bg",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1280,
					"height": 50
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-bg-ndddoddf.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1280,
						"height": 50
					}
				},
				"children": []
			}
		]
	}
]